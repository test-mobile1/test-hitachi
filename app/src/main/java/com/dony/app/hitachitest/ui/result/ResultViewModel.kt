package com.dony.app.hitachitest.ui.result

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import com.dony.app.hitachitest.repository.WeatherRepository
import com.dony.app.hitachitest.utils.Resource
import com.dony.app.hitachitest.utils.hasInternetConnection
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject


@HiltViewModel
class ResultViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    @ApplicationContext private val context: Context
) : ViewModel() {


    //variable that will listen to user's input
    var dataCelcius = MutableLiveData<String>("")
    var dataFahrenheit = MutableLiveData<String>("")
    //expose the variable to the owner(activity/fragment)
    val getDataCelcius: LiveData<String> = dataCelcius
    val getDataFahrenheit: LiveData<String> = dataFahrenheit


}