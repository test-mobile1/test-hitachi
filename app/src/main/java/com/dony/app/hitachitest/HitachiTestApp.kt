package com.dony.app.hitachitest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HitachiTestApp : Application() {
}