package com.dony.app.hitachitest.repository

import com.dony.app.hitachitest.data.WeatherAppService
import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepository @Inject constructor(
    private val weatherAppService: WeatherAppService
) {

    suspend fun postWeatherCurrent(key: String, query: String): Response<WeatherCurrentResponse> =
        withContext(
            Dispatchers.IO
        ) {
            val weatherCurrentResponse = weatherAppService.postWeatherCurrent(key, query)
            weatherCurrentResponse
        }
}