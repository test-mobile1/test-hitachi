package com.dony.app.hitachitest.ui.result

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.dony.app.hitachitest.R
import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import com.dony.app.hitachitest.databinding.ActivityResultBinding
import com.dony.app.hitachitest.ui.main.MainActivity.Companion.EXTRA_WEATHER_CURRENT
import com.dony.app.hitachitest.utils.contentView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResultActivity : AppCompatActivity() {

    private val binding: ActivityResultBinding by contentView(R.layout.activity_result)
    private val resultViewModel: ResultViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = resultViewModel
        binding.lifecycleOwner = this
        binding.activity = this

        binding.run {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = ContextCompat.getColor(applicationContext, R.color.white)
        }


        val weatherCurrentResponse =
            intent.getParcelableExtra(EXTRA_WEATHER_CURRENT) ?: WeatherCurrentResponse()

        resultViewModel.dataCelcius.value = weatherCurrentResponse.current?.tempC?.toString() + "\u2103"
        resultViewModel.dataFahrenheit.value = weatherCurrentResponse.current?.tempF?.toString() + "\u2109"

    }
}