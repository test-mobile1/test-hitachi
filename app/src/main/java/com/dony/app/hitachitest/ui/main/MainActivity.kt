package com.dony.app.hitachitest.ui.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.dony.app.hitachitest.BuildConfig
import com.dony.app.hitachitest.R
import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import com.dony.app.hitachitest.databinding.ActivityMainBinding
import com.dony.app.hitachitest.ui.result.ResultActivity
import com.dony.app.hitachitest.utils.Resource
import com.dony.app.hitachitest.utils.contentView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by contentView(R.layout.activity_main)
    private val mainViewModel: MainViewModel by viewModels()

    companion object {
        const val EXTRA_WEATHER_CURRENT = "EXTRA_WEATHER_CURRENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = mainViewModel
        binding.lifecycleOwner = this
        binding.run {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = ContextCompat.getColor(applicationContext, R.color.white)
        }
        mainViewModel.apiKeyInput.value = resources.getString(R.string.WEATHER_API_KEY);
        observeUI()
    }


    private fun observeUI() {
        mainViewModel.weatherCurrent.observe(this) {
            when (it) {
                is Resource.Success -> {
                    mainViewModel.updateLoading(false)
                    it.data?.let { dataWeatherCurrent ->
                        startActivity(launchResultScreen(this, dataWeatherCurrent))
                    }
                }
                is Resource.Error -> {
                    mainViewModel.updateLoading(false)
                    it.message?.let { message ->
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                    }
                }
                is Resource.Loading -> {
                    mainViewModel.updateLoading(true)
                }
            }

        }
    }


    private fun launchResultScreen(
        context: Context,
        weatherCurrentResponse: WeatherCurrentResponse
    ): Intent {
        val intent = Intent(context, ResultActivity::class.java)
        intent.putExtra(EXTRA_WEATHER_CURRENT, weatherCurrentResponse)
        return intent
    }


}