package com.dony.app.hitachitest.ui.main

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import com.dony.app.hitachitest.repository.WeatherRepository
import com.dony.app.hitachitest.utils.Resource
import com.dony.app.hitachitest.utils.hasInternetConnection
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    @ApplicationContext private val context: Context
) : ViewModel() {


    //variable that will listen to user's input
    var apiKeyInput = MutableLiveData<String>()
    //expose the variable to the owner(activity/fragment)
    val getApiKeyInput: LiveData<String> = apiKeyInput


    var isLoading = MutableLiveData<Boolean>(false)
    fun updateLoading(_loading:Boolean) {
        isLoading.value = _loading
    }

    private var selectedCountry: MutableLiveData<String>? = MutableLiveData<String>()
    fun getSelectedCountry(): MutableLiveData<String>? {
        return selectedCountry
    }


    val weatherCurrent: MutableLiveData<Resource<WeatherCurrentResponse>> = MutableLiveData()
    fun postWeatherCurrent() {
        weatherCurrent.postValue(Resource.Loading())
        viewModelScope.launch {
            try {
                if (hasInternetConnection(context)) {
                    val response = weatherRepository.postWeatherCurrent(getApiKeyInput.value!!, selectedCountry?.value.toString())
                    weatherCurrent.postValue(Resource.Success(response.body()!!))
                } else
                    weatherCurrent.postValue(Resource.Error("No Internet Connection"))
            } catch (ex: Exception) {
                when (ex) {
                    is IOException -> weatherCurrent.postValue(Resource.Error("Network Failure " + ex.localizedMessage))
                    else -> weatherCurrent.postValue(Resource.Error("Conversion Error"))
                }
            }
        }
    }

}