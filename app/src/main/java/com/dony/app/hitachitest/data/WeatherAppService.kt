package com.dony.app.hitachitest.data

import com.dony.app.hitachitest.data.models.WeatherCurrentResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface WeatherAppService {

    companion object {
        const val ENDPOINT = "http://api.weatherapi.com/v1/"
    }

    @FormUrlEncoded
    @POST("current.json")
    suspend fun postWeatherCurrent(
        @Field("key") apiKey: String?,
        @Field("q") query: String?
    ): Response<WeatherCurrentResponse>

}